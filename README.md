# DLA finder algorithm for D/H candidates

[![License MIT](https://img.shields.io/badge/License-MIT-blue.svg)](http://opensource.org/licenses/MIT)
[![PyPI version](https://badge.fury.io/py/llabs.svg)](https://badge.fury.io/py/llabs)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.438085.svg)](https://doi.org/10.5281/zenodo.438085)

## Description

This program looks for possible D/H candidates in a list of quasar spectra.

## Installation

This programs can be easily installed using [https://pypi.python.org/pypi/pip](pip) as follows

```bash
sudo pip install llabs
```

## Usage

The usage is very simple and is the following:

```bash
LLabs <input> --options
```

There are 3 different types of input, the user can either call a *single quasar spectrum* or a *list of several spectra*, this will run the scanning procedure. Also, it is possible to call the resulting *log file* in order to run the statistical plots.

## Help

```
[~]% llabs -h
usage: llabs [-h] [--bin] [--dlafit] [--dlasearch] [--dvlyamax] [--dvtrans]
             [--irange] [--limshift] [--limsnr] [--manual] [--maskwidth]
             [--metals] [--nodla] [--noplot] [--replot] [--reset] --resolution
             {low,high} [--reval] [--stdev] [-v] [--zabs]
             {scan,replot,update,metallicity,hist,regions,key,stat,metals,multi,allstats}
             filename

Damped Lyman-Alpha systems finder.

positional arguments:
  {scan,replot,update,metallicity,hist,regions,key,stat,metals,multi,allstats}
                        Operation to be performed.
  filename              List of quasar spectra, output log file, or single
                        quasar spectrum path.

optional arguments:
  -h, --help            show this help message and exit
  --bin                 Change binning for histogram
  --dlafit              Fit the DLA candidate
  --dlasearch           Search for DLA candidate
  --dvlyamax            Maximum velocity dispersion for Lyman alpha line
  --dvtrans             Velocity dispersion of higher Lyman transitions
  --irange              Pixel interval over which fluctuations are evaluated
  --limshift            Shift upper limit for unphysically large shifts
  --limsnr              SNR lower limit to discard noisy candidates
  --manual              Manually select the edges and the limit Lyman
  --maskwidth           Additional fraction of the saturated region
  --metals              Provide list of metal transitions for statistical
                        plots
  --nodla               Do not look for Damped Lyman system
  --noplot              Don't plot the figures
  --replot              Plot the figures based on the log file results
  --reset               Uncomment infile.dat content and re-scan the spectra
  --resolution {low,high}
                        Spectrograph used
  --reval               Re-evaluate SNR and metal absorption for each system
  --stdev               Standard deviation threshold for significant SNR
  -v, --verbose         Do verbose
  --zabs                Specify DLA absorption redshift

NOTE: In order to stop scanning the spectra from the infile.dat, you can type
"touch stop" in the terminal in the running directory, the loop will stop
after completion of the currently processed spectrum.
```

## Acknowledgment

Please acknowledge the use of this software in resulting publications by using the
following citation:

```text
@misc{vincent_dumont_2017_438085,
  author       = {Vincent Dumont},
  title        = {{LLabs v1.0.0 - DLA finder algorithm for D/H 
                   candidates}},
  month        = mar,
  year         = 2017,
  doi          = {10.5281/zenodo.438085},
  url          = {https://doi.org/10.5281/zenodo.438085}
}
```

## License

(The MIT License)

Copyright (c) 2016-2017 Vincent Dumont

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
