# System
import os
import logging

# External
import numpy
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import astropy.io.fits as fits
from scipy.ndimage import gaussian_filter1d

# Local
from .plots.fitting import p_voigt
from .constants import *

def readspec(filename,resolution):
    '''
    Read quasar spectrum.
    '''
    logging.info('|- Loading spectrum...')
    if filename.endswith('fits'):
        hdu = fits.open(filename)
        d   = hdu[0].data
        hd  = hdu[0].header
        if resolution=='low':
            hdu0 = hdu[0].header
            hdu1 = hdu[1].header
            wa = numpy.array(10.**(hdu0['coeff0'] + hdu0['coeff1'] * numpy.arange(hdu1['naxis2'])))
            co = numpy.array(hdu[1].data['model'])
            fl = numpy.array(hdu[1].data['flux'])
            er = numpy.array([1/numpy.sqrt(hdu[1].data['ivar'][i]) if hdu[1].data['ivar'][i]!=0 else 10**32 for i in range (len(fl))])
        else:
            wa = 10**(hd['CRVAL1'] + (hd['CRPIX1'] - 1 + numpy.arange(hd['NAXIS1']))*hd['CDELT1'])
            co = d[3,:] # continuum estimate
            fl = d[0,:] # normalised flux
            er = d[1,:] # the normalised 1 sigma error
        badpixel = numpy.append(numpy.where(er>1.e10)[0],numpy.where(fl==1.)[0])
        wa = numpy.delete(wa,badpixel,0)
        co = numpy.delete(co,badpixel,0)
        fl = numpy.delete(fl,badpixel,0)
        er = numpy.delete(er,badpixel,0)
        name = filename.split('/')
        qsoname = name[-1].replace('.fits','')
    elif filename.endswith('dat'):
        d = numpy.genfromtxt(filename)
        wa = d[:,0]
        fl = d[:,1] # normalised flux
        er = d[:,2] # the normalised 1 sigma error
        if numpy.average(er) == 0:
            er.fill(.01)
        badpixel = numpy.append( (numpy.where(abs(er) < 1e-10)[0]) , (numpy.where(abs(er) > 1e+10)[0]))
        wa = numpy.delete(wa,badpixel,0)
        fl = numpy.delete(fl,badpixel,0)
        er = numpy.delete(er,badpixel,0)
        name = filename.split('/')
        qsoname = name[-1].replace('.dat','')
    else:
        '''
        Read simulated ASCII spectrum from qsosim9. Only the second spectrum is read.
        There's no noise in the spectra so it's randomly generated
        specfile:   filename
        '''
        d = numpy.genfromtxt(filename)
        wa = d[:,0]
        co = numpy.ndarray((len(wa),))
        co.fill(1)              # continuum estimate
        fl = d[:,1]             # normalised flux
        er = d[:,2]             # the normalised 1 sigma error
        badpixel = numpy.append( (numpy.where(abs(er) < 1e-10)[0]) , (numpy.where(abs(er) > 1e+10)[0]))
        wa = numpy.delete(wa,badpixel,0)
        co = numpy.delete(co,badpixel,0)
        fl = numpy.delete(fl,badpixel,0)
        er = numpy.delete(er,badpixel,0)
        name = filename.split('/')
        qsoname = name[-1].replace('.dat','')
    data = {'wa':wa,'co':co,'fl':fl,'er':er,'qsoname':qsoname}
    return data

def equiwidth(data,zalpha,dlawidth,stdev,**kwargs):
    '''
    Determine the absorption equivalent width around metal lines within the DLA velocity dispersion.
    Determine the fraction of pixels with absorption around metal lines
    CHECK OUT THE DEFINITION OF DELTAV in http://arxiv.org/pdf/astro-ph/0606185v1.pdf,better described
    here http://arxiv.org/pdf/1303.7239v3.pdf. Not sure it's possible to use here, since they visually
    inspect to require the lines aren't saturated...
    '''
    logging.info('|- Evaluate equivalent width of metal transitions :')
    zmin = zalpha * (2*c-dlawidth/2) / (2*c+dlawidth/2) - dlawidth / (2*c-dlawidth/2)
    zmax = zalpha * (2*c+dlawidth/2) / (2*c-dlawidth/2) + dlawidth / (2*c-dlawidth/2)
    for trans in Metallist:
        wamin  = trans['Metalwave']*(zmin+1)
        wamax  = trans['Metalwave']*(zmax+1)
        imin   = abs(data['wa']-wamin).argmin()
        imax   = abs(data['wa']-wamax).argmin()
        if imin >= imax:
            logging.info('|  |- {:<5} {:>7.2f} : Not in range'.format(trans['Metalline'],trans['Metalwave']))
        else:
            ymax   = sorted(data['fl'][imin:imax])[int(0.99*(imax-imin))]
            metabs = 0
            for i in range(imin,imax-1):
                flux   = 0 if data['fl'][i]<=0 else 1 if data['fl'][i]>=ymax else data['fl'][i]/ymax
                metabs += (1-flux)*(data['wa'][i+1]-data['wa'][i])/(1+(zmin+zmax)/2)
            index   = numpy.where(data['fl'][imin:imax] < 1. - stdev*data['er'][imin:imax])
            metfrac = 100*float(len(index[0]))/float(imax-imin) if imin<imax else 0
            logging.info('|  |- {:<5} {:>7.2f} : {:.2f} ( {:3} % )'.format(trans['Metalline'],trans['Metalwave'],metabs,round(metfrac)))
            
def snrestimator(data,llhard,lldla,waalpha,**kwargs):
    '''
    Estimate signal-to-noise ratio from spectrum.
    '''
    wmin = lldla if lldla!=None else llhard
    wmax = waalpha if lldla!=None else llhard+1000
    snr  = []
    ibeg = abs(data['wa'] - wmin).argmin()
    iend = abs(data['wa'] - wmax).argmin()
    for j in range(ibeg,iend):
        if numpy.isnan(data['fl'][j])==numpy.isnan(data['er'][j])==False:
            snr.append(data['fl'][j]/data['er'][j])
    logging.info('|- Estimated signal-to-noise ratio (SNR) : %.2f' % numpy.average(snr))

def best(filename,output='llabs.csv',**kwargs):
    os.makedirs(os.path.dirname(os.path.abspath(output)),exist_ok=True)
    out = {'qso':[],'npix1a':[],'npix1b':[],'npix2a':[],'npix2b':[],
           'nleftHI':[],'nrightHI':[],'llfuzzy':[],'llhard':[],'lldla':[],'llshift':[],
           'dlawidth':[],'zalpha':[],'N':[],'b':[],'snr':[]}
    log_file = numpy.loadtxt(filename,dtype=str,delimiter='\n')
    npix1a = int(log_file[3].split()[-9])
    npix1b = int(log_file[4].split()[-9])
    npix2a = int(log_file[5].split()[-9])
    npix2b = int(log_file[6].split()[-9])
    for i,line in enumerate(log_file):
        if 'Processing spectrum' in line:
            qso = line.split()[-1]
        if 'Minimum redshift' in line:
            nleftHI = line.split()[-2]
        if 'Maximum redshift' in line:
            nrightHI = line.split()[-2]
        if 'Fuzzy limit' in line:
            llfuzzy = line.split()[-2]
        if 'Hard limit' in line:
            llhard = float(line.split()[-2])
        if 'Lyman limit wavelength' in line:
            lldla = float(line.split()[-2])
        if 'Velocity width' in line:
            dlawidth = line.split()[-2]
        if 'Absorption redshift' in line:
            zalpha = line.split()[-1]
        if 'Column density' in line:
            N = line.split()[-7]
        if 'Doppler parameter' in line:
            b = line.split()[-8]
        if 'LL apparent shift' in line:
            llshift = line.split()[-2]
            snr = log_file[i+1].split()[-1]
            for key in out.keys():
                out[key].append(eval(key))
    df = pd.DataFrame.from_dict(out)
    df.to_csv(output, index=False, header=True)

def make_lookup_tables(disp=0.0025,threshold=0.005):
    """
    Tabulate width of saturated region for different profiles in N,b parameter space.
    Create, for each order, an ASCII file with the N,b parameter space since the FWHM changes
    for different b values at higher orders of the Lyman series. For each ASCII file, each row
    represents the column density, and each column, the b values.
    """
    os.makedirs('data',exist_ok=True)
    dv = 20000
    for i,trans in enumerate(HIlist):
        print('HI-%i' % i)
        op  = open('data/HI%i.dat' % i,'w')
        for N in cols:
            print('\t%.2f' % N)
            for b in dops:
                ref    = trans['wave']
                wmin   = ref * (1 - (dv/c))
                wmax   = ref * (1 + (dv/c))
                wave   = numpy.arange(wmin,wmax,disp)
                vprof  = p_voigt(N,b,wave,trans['wave'],trans['gamma'],trans['strength'])
                flux   = gaussian_filter1d(vprof,1.5)
                center = abs(wave-ref).argmin()
                first  = flux[0:center]
                second = flux[center:(len(flux)-1)]
                a      = abs(first - threshold).argmin()
                b      = abs(second - threshold).argmin()
                width  = (wave[center+b]-wave[a])/wave[a]*c
                op.write(str('{:>15}'.format('%.4f'%width)))
            op.write('\n')
        op.close()
