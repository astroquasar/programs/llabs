# System
import logging

# External
import numpy
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

# Local
from .constants import *

class ManualScan(object):
    '''
    Perform manual scanning using Python interface.
    '''
    def __init__(self,**kwargs):
        self.waalpha = None
        self.lldla = None
        self.llfuzzy = None
        self.llhard = None
        self.dvshift = None
        self.dlawidth = None
        self.zalpha = None
        self.zlimit = None
        self.llflag, self.left, self.right = False, False, False
    
    def search(self,resolution,data,qsoname,npix1a,npix1b,npix2a,npix2b,stdev=2.5,irange=10,**kwargs):
        self.qsoname = qsoname
        self.wa,self.fl,self.er = data['wa'],data['fl'],data['er']
        mpl.matplotlib_fname()
        # Empty keymap for interactive tool
        keyMaps = [key for key in plt.rcParams.keys() if 'keymap.' in key and key!='keymap.all_axes']
        for keyMap in keyMaps: plt.rcParams[keyMap]=''
        logging.info('|- Start manual scanning...')
        logging.info('|- Looking for Lyman limit...')
        self.fig = plt.figure(figsize=(12,8))
        plt.subplots_adjust(left=0.05, right=0.95, bottom=0.05, top=0.95, hspace=0, wspace=0.1)
        self.llsearch(data)
        self.fig.canvas.mpl_connect('key_press_event', self.press)
        plt.show()
        if self.llflag==False:
            logging.info('|- No observable Lyman limit found')
        elif self.lldla==None:
            logging.info('|- No suitable DLA Lyman-alpha found!')

    def press(self,event):
    
        if event.xdata!=None:
            self.spidx(event)
            if event.key=='?':
                print('|- Show commands for manual detection:')
                print('|  ? - List of commands')
                print('|  w - Select the position of the Lyman limit')
                print('|  r - Select right edge of the DLA region')
                print('|  l - Select left edge of the DLA region')
                print('|  c - Confirm Lyman limit / DLA positions')
                print('|  a - Zoom in from y axis')
                print('|  z - zoom out from y axis')
                print('|  s - zoom in from x axis')
                print('|  x - zoom out from x axis')
                print('|  q - Leave the program')
            if event.key=='w' and self.llflag==False:
                self.selreg(event,'limit')
                self.fig.canvas.draw()
            if event.key=='c':
                if self.llflag==False and self.zlimit!=None:
                    self.llflag = True
                    logging.info('|- Lyman Limit selected :')
                    logging.info('|  |- Hard limit  : %.2f A' % self.llhard)
                    logging.info('|  |- LL redshift : %.5f' % self.zlimit)
                    self.plottrans()
                    self.fig.canvas.draw()
                if self.left==True and self.right==True and self.lldla==None:
                    self.zmax     = self.zlimit * (2*c+self.dvmax) / (2*c-self.dvmax) + 2*self.dvmax / (2*c-self.dvmax)
                    self.dlawidth = self.dvmax - self.dvmin
                    self.nrightHI = self.axisNr if self.axisNr<31 else self.axisNr-31
                    self.lastHI   = max([self.nleftHI,self.nrightHI])
                    self.zalpha   = (self.zmin+self.zmax)/2
                    self.waalpha  = HIlist[0]['wave']*(self.zalpha+1)
                    self.lldla    = (self.zalpha+1.)*HIlist[-1]['wave']
                    self.dvshift  = 2*(self.llhard-self.lldla)/(self.llhard+self.lldla)*c
                    logging.info('|- Best DLA candidate:')
                    logging.info('|  |- Absorption redshift    : %.5f' % self.zalpha)
                    logging.info('|  |- Lyman limit wavelength : %.2f A' % self.lldla)
                    logging.info('|  |- Velocity width         : %.2f km/s' % self.dlawidth)
                    logging.info('|  |- Minimum redshift       : %.5f ( left wing found at Ly %i )' % (self.zmin,self.nleftHI))
                    logging.info('|  |- Maximum redshift       : %.5f ( right wing found at Ly %i )' % (self.zmax,self.nrightHI))
                    logging.info('|  |- LL apparent shift      : %.2f km/s' % self.dvshift)                    
            if event.key=='l' and self.lldla==None:
                self.selreg(event,'left')
                self.plottrans()
                self.fig.canvas.draw()
            if event.key=='r' and self.lldla==None:
                self.selreg(event,'right')
                self.plottrans()
                self.fig.canvas.draw()
            if event.key=='a':
                ymin, ymax = self.ax.get_ylim()
                tenper = (ymax-ymin)/10.
                if ymin+tenper<ymax-tenper:
                    self.ax.set_ylim(ymin+tenper,ymax-tenper)
                    self.fig.canvas.draw()
            if event.key=='z':
                ymin, ymax = self.ax.get_ylim()
                tenper = (ymax-ymin)/10.
                self.ax.set_ylim(ymin-tenper,ymax+tenper)
                self.fig.canvas.draw()
            if event.key=='s':
                xmin, xmax = self.ax.get_xlim()
                tenper = (xmax-xmin)*10./100.
                self.ax.set_xlim(event.xdata-tenper,event.xdata+tenper)
                self.fig.canvas.draw()
            if event.key=='x':
                xmin, xmax = self.ax.get_xlim()
                tenper = (xmax-xmin)*100./20.
                self.ax.set_xlim(event.xdata-tenper/2,event.xdata+tenper/2)
                self.fig.canvas.draw()
            if event.key=='q':
                plt.close()
    
    def llsearch(self,data):
    
        ax = plt.subplot(111)
        ax.plot(self.wa,self.fl,lw=0.8,alpha=0.8,color='black')
        ax.plot(self.wa,self.er,lw=0.8,alpha=0.8,color='cyan')
        ax.set_ylim(0,1)
        self.ax = ax
    
    def plottrans(self):
    
        plt.clf()
        self.axlist = []
        for idx in range(len(HIlist)):
            wacent  = float(HIlist[idx]['wave'])*(self.zlimit+1)
            v       = c*((self.wa-wacent)/wacent)
            istart  = abs(v-(-2000)).argmin()
            iend    = abs(v-(+3000)).argmin()
            ymax    = 1 if istart==iend else sorted(self.fl[istart:iend])[int(0.95*(iend-istart))]
            ax      = plt.subplot(31,2,1+2*idx) if idx==0 else plt.subplot(31,2,1+2*idx,sharex=ax)
            ax.set_xlim(-5000,0)
            ax.set_ylim(0,ymax)
            ax.yaxis.set_major_locator(ticker.NullLocator())
            if idx<len(HIlist)-1:
                ax.xaxis.set_major_locator(ticker.MultipleLocator(500))
            else:
                ax.xaxis.set_major_locator(ticker.NullLocator())
            ax.plot(v,self.fl,lw=0.8,alpha=0.8,color='black')
            ax.plot(v,self.er,lw=0.8,alpha=0.8,color='cyan')
            ax.axvline(x=0,lw=3,color='red')
            ax.axvline(x=self.dvmin,lw=2,color='green')
            ax.axvline(x=self.dvmax,lw=2,color='green')
            ax.axvspan(self.dvmin,self.dvmax,facecolor='lime',alpha=0.5,ec='none')
            metal = 'HI_'+str(HIlist[idx]['wave']).split('.')[0]
            ax.set_ylabel(metal,rotation='horizontal',ha='right')
            self.axlist.append(ax)
        for idx in range(len(Metallist)):
            wacent  = float(Metallist[idx]['Metalwave'])*(self.zlimit+1)
            v       = c*((self.wa-wacent)/wacent)
            istart  = abs(v-(-2000)).argmin()
            iend    = abs(v-(+3000)).argmin()
            ymax    = 1 if istart==iend else sorted(self.fl[istart:iend])[int(0.95*(iend-istart))]
            ax      = plt.subplot(31,2,2+2*idx) if idx==0 else plt.subplot(31,2,2+2*idx,sharex=ax)
            ax.set_xlim(-5000,0)
            ax.set_ylim(0,ymax)
            ax.yaxis.set_major_locator(ticker.NullLocator())
            if idx<len(HIlist)-1:
                ax.xaxis.set_major_locator(ticker.MultipleLocator(500)) 
            else:
                ax.xaxis.set_major_locator(ticker.NullLocator())
            ax.plot(v,self.fl,lw=0.8,alpha=0.8,color='black')
            ax.plot(v,self.er,lw=0.8,alpha=0.8,color='cyan')
            ax.axvline(x=0,lw=3,color='red')
            ax.axvline(x=self.dvmin,lw=2,color='green')
            ax.axvline(x=self.dvmax,lw=2,color='green')
            ax.axvspan(self.dvmin,self.dvmax,facecolor='lime',alpha=0.5,ec='none')
            metal = Metallist[idx]['Metalline']+'_'+str(Metallist[idx]['Metalwave']).split('.')[0]
            ax.set_ylabel(metal,rotation='horizontal',ha='right')
            self.axlist.append(ax)
        self.fig.suptitle(self.qsoname,fontsize=12)
    
    def selreg(self,event,action):
    
        if action=='limit':
            self.llhard = event.xdata
            self.zlimit  = (self.llhard/HIlist[-1]['wave'])-1.
            self.zmin    = self.zmax = self.zlimit
            self.dvmin   = self.dvmax = 0
            self.nleftHI = self.nrightHI = 0
            xmin   = self.llhard - 15
            xmax   = self.llhard + 15
            istart = abs(self.wa-xmin).argmin()
            iend   = abs(self.wa-xmax).argmin()
            ymax   = max(self.fl[istart:iend])
            ax     = self.ax
            ax.clear()
            ax.plot(self.wa,self.fl,lw=0.8,alpha=0.8,color='black')
            ax.plot(self.wa,self.er,lw=0.8,alpha=0.8,color='cyan')
            ax.set_xlim(self.llhard-10,self.llhard+10)
            ax.set_ylim(-0.1,ymax)
            ax.axvline(x=self.llhard,color='red',lw=3)
            self.ax = ax
        if action=='left':
            self.left     = True
            self.dvmin    = event.xdata
        if action=='right':
            self.right    = True
            self.dvmax    = event.xdata
        
    def spidx(self,event):
    
        i = 0
        self.axisNr = None
        for axis in self.fig.axes:
            if axis == event.inaxes:
                self.axisNr = i
                break
            i += 1
    
