# System
import os
import re
import sys
import logging
import logging.config

# External
import numpy
import pandas as pd

# Local
from .auto import AutoScan
from .manual import ManualScan
from .plots import llplot, allregions, PlotDLA, wholespec
from .utils import readspec, equiwidth, snrestimator

def settings(resolution,npix1a,npix1b,npix2a,npix2b,threshold1,threshold2,stdev=None,irange=None,vstart=0,limshift=10000,dvtrans=50,dvlyamax=20000,**kwargs):
    params = {
        'limshift'   : limshift,
        'dvtrans'    : dvtrans,
        'dvlyamax'   : dvlyamax,
        'vstart'     : vstart,
        'npix1a'     : npix1a     if     npix1a!=None else  50 if resolution=='low' else 250,
        'npix1b'     : npix1b     if     npix1b!=None else 100 if resolution=='low' else 500,
        'npix2a'     : npix2a     if     npix2a!=None else  20 if resolution=='low' else 100,
        'npix2b'     : npix2b     if     npix2b!=None else  40 if resolution=='low' else 200,
        'stdev'      : stdev      if      stdev!=None else   4 if resolution=='low' else 2.5,
        'irange'     : irange     if     irange!=None else   3 if resolution=='low' else 10,
        'threshold1' : threshold1 if threshold1!=None else   7 if resolution=='low' else 10,
        'threshold2' : threshold2 if threshold2!=None else   5,
    }
    logging.info('='*60)
    logging.info('Execute LLabs on %s-resolution quasar spectra' % resolution.capitalize())
    logging.info('='*60)
    logging.info('  npix1a : {:3}  |  irange     : {:4}  |  vstart   : {:5}'.format(int(params['npix1a']),int(params['irange']),int(params['vstart'])))
    logging.info('  npix1b : {:3}  |  stdev      : {:4.1f}  |  dvtrans  : {:5}'.format(int(params['npix1b']),float(params['stdev']),int(params['dvtrans'])))
    logging.info('  npix2a : {:3}  |  threshold1 : {:4}  |  dvlyamax : {:5}'.format(int(params['npix2a']),int(params['threshold1']),int(params['dvlyamax'])))
    logging.info('  npix2b : {:3}  |  threshold2 : {:4}  |  limshift : {:5}'.format(int(params['npix2b']),int(params['threshold2']),int(params['limshift'])))
    logging.info('='*60)
    merge_dict = {**params,**kwargs}
    return merge_dict

def scan(resolution,filename=None,qsolist=None,reset=False,manual=False,fit=False,
         plot=False,metal=False,log_file='llabs.log',verbose=False,**kwargs):
    assert qsolist!=None or filename!=None, \
        'No spectrum (-f,--filename) or file list (-l,--qsolist) provided. Abort'
    # Create logging file
    logging.config.dictConfig({'version':1,'disable_existing_loggers': True})
    log_format = '%(asctime)s %(levelname)s %(message)s'
    log_level = logging.DEBUG if verbose else logging.INFO
    stream_handler = logging.StreamHandler(stream=sys.stdout)
    stream_handler.setLevel(log_level)
    handlers = [stream_handler]
    os.makedirs(os.path.dirname(os.path.abspath(log_file)),exist_ok=True)
    file_handler = logging.FileHandler('%s' % log_file, mode='w')
    file_handler.setLevel(log_level)
    handlers.append(file_handler)
    logging.basicConfig(level=log_level, format=log_format, handlers=handlers)
    params = settings(resolution,**kwargs)
    if qsolist!=None:
        if reset: os.system("sed -i '' 's,!,,' "+qsolist)
        qsolist = numpy.loadtxt(qsolist,dtype=str,comments='!',ndmin=1)
    else:
        qsolist = [filename]
    for i,spectrum in enumerate(qsolist):
        qsoname = re.split(r'[/.]',spectrum)[-2]
        logging.info('\t%i/%i - Processing spectrum : %s' % (i+1,len(qsolist),spectrum))
        logging.info('='*60)
        if os.path.exists(spectrum)==False:
            logging.info('|  |- Spectrum not found!')
            continue
        data = readspec(spectrum,resolution)
        if len(data['wa'])==0:
            logging.info('|  |- Zero flux everywhere, spectrum not usable!')
            continue
        if manual:
            scan = ManualScan()
            scan.search(resolution,data,qsoname,**params)
        else:
            logging.info('|- Scanning spectrum...')
            scan = AutoScan()
            scan.llfind(resolution,data,**params)
        if metal and scan.lldla!=None:
            equiwidth(data,**vars(scan))
        if scan.llhard!=None:
            snrestimator(data,**vars(scan))
        if plot:
            if scan.llhard!=None:
                llplot(qsoname,data,**vars(scan))
            if scan.lldla!=None:
                PlotDLA(qsoname,data,fit,**vars(scan))
            else:
                wholespec(qsoname,data,**vars(scan))
        logging.info('|- End of scanning.')
        logging.info('='*60)
        if filename=='qsolist.dat':
            os.system("sed -i '' 's,"+spectrum+",!"+spectrum.replace('!','')+",' "+filename)
        if os.path.exists('./stop')==True:
            os.system('rm stop')
            break

def plot(csv_file,resolution,limit=False,dla=False,**kwargs):
    df = pd.read_csv(csv_file,header=0)
    if dla or dla==limit==False:
        for i in range(len(df)):
            data = readspec(df['qso'][i],resolution)
            qsoname = re.split(r'[/.]',df['qso'][i])[-2]
            print('{:>3}/{:<3} - {}'.format(i+1,len(df),qsoname))
            PlotDLA(qsoname,data,**df.loc[i])
    if limit or dla==limit==False:
        allregions(df,resolution)
