# System
import os
import logging

# External
import matplotlib.pyplot as plt

def wholespec(qsoname,data,npix1a,npix1b,npix2a,npix2b,llfuzzy=None,llhard=None,save=True,**kwargs):
    '''
    Plot the spectrum region from the detected Lyman-limit to the detected Lyman-alpha

    Parameters
    ----------
    fig   : Figure to be plotted on
    Ncol  : Number of columns to plot over
    Nplot : Plot number
    '''
    logging.info('|- Plotting whole spectrum...',)
    plt.style.use('seaborn')
    fig = plt.figure(figsize=(8.27,11.69))
    plt.axis('off'), plt.xticks(()), plt.yticks(())
    ymin, ymax = 0, 1
    Ncol    = 1
    Nrows   = 10
    waveint = (data['wa'][-1]-data['wa'][0])/Nrows
    wmin    = data['wa'][0]
    wmax    = data['wa'][0] + waveint
    istart  = 0
    iend    = abs(data['wa'] - wmax).argmin()
    for i in range(1,Nrows+1):
        x = data['wa'][istart:iend]
        y = data['fl'][istart:iend]
        if istart!=iend:
            ymax = 1.1*sorted(y)[int(0.99*(iend-istart))]
            ymin = -ymax*0.1/1.1
        ax = fig.add_subplot(Nrows,Ncol,i,xlim=[wmin,wmax],ylim=[ymin,ymax])
        ax.plot(x,data['er'][istart:iend],'cyan',lw=0.2,zorder=2)
        if llfuzzy!=None:
            ilim = abs(data['wa']-llfuzzy).argmin()
            ax.axvline(x=llfuzzy,color='magenta',lw=2,alpha=0.8,zorder=3)
            ax.axvspan(data['wa'][max(0,ilim-npix1b)],data['wa'][min(len(data['wa'])-1,ilim+npix1a)],color='magenta',lw=0,alpha=0.4,zorder=4)
        if llhard!=None:
            ilim = abs(data['wa']-llhard).argmin()
            ax.axvline(x=llhard,color='red',lw=2,alpha=0.8,zorder=5)
            ax.axvspan(data['wa'][ilim-npix2b],data['wa'][ilim+npix2a],color='red',lw=0,alpha=0.4,zorder=6)
        ax.plot(x,y,'black',lw=0.7,zorder=1)
        wmin   = wmax
        wmax   = wmin+waveint
        istart = iend
        iend   = abs(data['wa'] - wmax).argmin()
    plt.tight_layout()
    if save:
        os.makedirs('candidates/spec',exist_ok=True)
        plt.savefig('./candidates/spec/%s.pdf' % qsoname)
        plt.close()
    else:
        plt.show()

