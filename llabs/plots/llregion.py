# System
import re
import logging

# External
import numpy
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from matplotlib.backends.backend_pdf import PdfPages

# Local
from ..utils import readspec
from ..constants import *

def llplot(qsoname,data,npix1a,npix1b,npix2a,npix2b,llhard,llfuzzy,dv_neg=-5000,dv_pos=7500,save=True,**kwargs):
    '''
    Plot of Lyman limit region for all systems in log file.
    '''
    logging.info('|- Creating plot of Lyman limit region...')
    wastart = float(llhard)*(2*c+dv_neg)/(2*c-dv_neg)
    waend   = float(llhard)*(2*c+dv_pos)/(2*c-dv_pos)
    istart  = abs(data['wa'] - wastart).argmin()
    iend    = abs(data['wa'] - waend).argmin()
    ymax    = 1 if istart==iend else sorted(data['fl'][istart:iend])[int(0.99*(iend-istart))]
    plt.style.use('seaborn')
    fig = plt.figure(figsize=(12,4),dpi=100)
    ilim = abs(data['wa']-float(llfuzzy)).argmin()
    plt.axvline(x=llfuzzy,color='red',lw=3,alpha=0.5,ls='dashed')
    plt.axvspan(data['wa'][max(0,ilim-int(npix1b))],data['wa'][min(len(data['wa'])-1,ilim+int(npix1a))],color='lightsalmon',alpha=0.7,lw=0,zorder=1)
    ilim = abs(data['wa']-float(llhard)).argmin()
    plt.axvline(x=llhard,color='red',lw=3,alpha=0.7)
    plt.axvspan(data['wa'][ilim-int(npix2b)],data['wa'][ilim+int(npix2a)],color='moccasin',alpha=0.7,lw=0,zorder=2)
    plt.plot(data['wa'],data['fl'],'black',lw=0.2,zorder=3)
    plt.plot(data['wa'],data['er'],'cyan',lw=0.2,zorder=3)
    plt.xlim(wastart,waend)
    plt.ylim(-0.25,ymax)
    plt.xlabel(r'Wavelength ($\mathrm{\AA}$)')
    plt.ylabel('Flux')
    plt.tight_layout()
    if save:
        os.makedirs('candidates/limit',exist_ok=True)
        plt.savefig('candidates/limit/%s.pdf' % qsoname)
        plt.close()
    else:
        plt.show()

def allregions(df,resolution,per_page=10,dv_neg=-5000,dv_pos=7500):
    '''
    Plot of Lyman limit region for all systems in log file.
    '''
    df = df.sort_values('llshift',ascending=True)
    pdf_pages = PdfPages('llregions.pdf')
    p = 0
    for i in range(0,len(df),per_page):
        f = 1
        fig = plt.figure(figsize=(8.27, 11.69))
        plt.axis('off')
        plt.subplots_adjust(left=0.05, right=0.95, bottom=0.06, top=0.96, wspace=0, hspace=0)
        for n in range(per_page):
            if i+n==len(df): break
            info = df.iloc[i+n]
            qsoname = re.split(r'[/.]',info['qso'])[-2]
            print('{:>3}/{:<3} - {}'.format(i+n+1,len(df),qsoname))
            data = readspec(info['qso'],resolution)
            p = p + 1
            wastart = float(info['lldla'])*(2*c+dv_neg)/(2*c-dv_neg)
            waend   = float(info['lldla'])*(2*c+dv_pos)/(2*c-dv_pos)
            istart  = abs(data['wa'] - wastart).argmin()
            iend    = abs(data['wa'] - waend).argmin()
            ymax    = 1 if istart==iend else sorted(data['fl'][istart:iend])[int(0.99*(iend-istart))]
            wafit   = numpy.arange(wastart,waend,0.05)
            ax = fig.add_subplot(per_page,1,f,xlim=[wastart,waend],ylim=[-ymax,ymax])
            ax.xaxis.tick_top()
            ax.xaxis.set_major_locator(ticker.NullLocator())
            ax.yaxis.set_major_locator(ticker.FixedLocator([0]))
            ilim = abs(data['wa']-float(info['llfuzzy'])).argmin()
            ax.axvline(x=info['llfuzzy'],color='green',lw=2,alpha=0.7)
            ax.axvspan(data['wa'][ilim-info['npix1b']],data['wa'][ilim+info['npix1a']],color='lime',lw=1,alpha=0.2,zorder=1)
            ilim = abs(data['wa']-float(info['llhard'])).argmin()
            ax.axvline(x=info['llhard'],color='blue',lw=2,alpha=0.7)
            ax.axvspan(data['wa'][ilim-info['npix2b']],data['wa'][ilim+info['npix2a']],color='blue',lw=1,alpha=0.2,zorder=1)
            ax.plot(data['wa'][istart:iend],data['fl'][istart:iend],'black',lw=0.2)
            ax.plot(data['wa'][istart:iend],data['er'][istart:iend],'cyan',lw=0.2)
            ax.axhline(y=0,ls='dotted',color='red',lw=0.2)
            ax.axvline(x=float(info['lldla']),color='red',lw=2,alpha=0.5)
            ax.axvline(x=float(info['llhard']),color='red',lw=2,alpha=0.5)
            t1 = plt.text(float(info['lldla']),0,info['lldla'],size=8,rotation=90,color='red',ha='center',va='center')
            t2 = plt.text(float(info['llhard']),0,info['llhard'],size=8,rotation=90,color='red',ha='center',va='center')
            t3 = plt.text(wastart+0.01*(waend-wastart),-0.50*ymax,info['qso'],size=8,color='blue',ha='left',va='center')
            t4 = plt.text(wastart+0.01*(waend-wastart),-0.75*ymax,'%i km/s'%info['llshift'],size=6,color='blue',ha='left',va='center')
            for t in [t1,t2,t3,t4]:
                t.set_bbox(dict(color='white', alpha=0.8, edgecolor=None))
            f = f + 1
        pdf_pages.savefig(fig)
        plt.close()
    pdf_pages.close()
