# System
import re
import logging

# External
import time
import numpy
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from scipy.ndimage import gaussian_filter1d

# Local
from .voigt import p_voigt
from .fitting import fitHI, residual
from ..utils import readspec
from ..constants import *

class PlotDLA():

    def __init__(self,filename,data,fit=False,save=True,**kwargs):
        """
        Wrap all subplots into single figure.
        """
        kwargs['N']=18
        logging.info('|- Creating plot of Damped Lyman Alpha region...')
        plt.style.use('seaborn')
        self.fig = plt.figure(figsize=(8.27,11.69),dpi=100)
        plt.axis('off'),plt.xticks(()),plt.yticks(())
        plt.subplots_adjust(left=0.05, right=0.95, bottom=0.01, top=0.93, hspace=0.1, wspace=0.05)
        if fit:
            self.fitmask, self.fitparams = fitHI(data,**kwargs)
        self.lyman_limit(data,filename,fit,vmin=-5000,vmax=5000,**kwargs)
        self.specplot(data,**kwargs)
        vmin,vmax = -2000,3000
        for Ntrans in range(0,31,1):
            Nplot=(2*Ntrans-1)+10
            self.Hplot(data,fit,Ntrans,Nplot=Nplot,vmin=vmin,vmax=vmax,**kwargs)
        vmin,vmax = -500,500
        for Ntrans in range(0,31,1):
            Nplot=(2*Ntrans-1)+11
            self.metalplot(data,Ntrans,Nplot=Nplot,vmin=vmin,vmax=vmax,**kwargs)
        if save:
            os.makedirs('candidates/dla',exist_ok=True)
            plt.savefig('./candidates/dla/%s.pdf' % filename)
            plt.close()
        else:
            plt.show()
    
    def lyman_limit(self,data,filename,fit,llhard,llfuzzy,zalpha,dlawidth,lldla,npix1a,npix1b,npix2a,npix2b,N,b,vmin=-10000.,vmax=10000.,Ncol=1,Nplot=1,**kwargs):
        '''
        Plot the spectrum region where the Lyman-limit is detected
    
        Parameters
        ----------
        fig   : Figure to be plotted on
        vmin  : Minimum velocity on plot in km/s
        vmax  : Maximum velocity on plot in km/s
        Ncol  : Number of columns to plot over
        Nplot : Plot number
        '''
        dvshift = round(2*(llhard-lldla)/(llhard+lldla)*c,2)
        llinfo  = '$\lambda_\mathrm{LL,obs}$ = %.2f $\AA$  ;  $\mathrm{dv}_\mathrm{LL,shift}$ = %.0f km/s'%(llhard,dvshift)
        dlainfo = '$z_\mathrm{abs}$ = %.5f  ;  $\mathrm{dv}_\mathrm{DLA}$ = %.0f km/s'%(zalpha,dlawidth)
        plt.title(filename+'  ;  '+llinfo+'  ;  '+dlainfo+'\n\n',fontsize=10)
        Nrows   = 23
        dv_neg  = -2500
        dv_pos  = 12000
        wastart = lldla*(2*c+dv_neg)/(2*c-dv_neg)
        waend   = lldla*(2*c+dv_pos)/(2*c-dv_pos)
        istart  = abs(data['wa'] - wastart).argmin()
        iend    = abs(data['wa'] - waend).argmin()
        wafit   = numpy.arange(wastart,waend,0.05)
        ymin,ymax = self.set_ylimit(data['fl'],istart,iend)
        ax = self.fig.add_subplot(Nrows,Ncol,Nplot,xlim=[wastart,waend],ylim=[ymin,ymax])
        ax.xaxis.tick_top()
        ax.xaxis.set_major_locator(plt.FixedLocator([]))
        ax.yaxis.set_major_locator(plt.FixedLocator([0]))
        ax.text(lldla,ymax+0.2*(ymax-ymin),'%.2f'%lldla,ha='right')
        ax.text(llhard,ymax+0.2*(ymax-ymin),'%.2f'%llhard,ha='left')
        ilim = abs(data['wa']-llfuzzy).argmin()
        ax.axvline(x=llfuzzy,color='green',lw=2,alpha=0.7)
        ax.axvspan(data['wa'][ilim-npix1b],data['wa'][ilim+npix1a],color='lime',lw=0,alpha=0.2,zorder=1)
        ilim = abs(data['wa']-float(llhard)).argmin()
        ax.axvline(x=llhard,color='blue',lw=2,alpha=0.7)
        ax.axvspan(data['wa'][ilim-npix2b],data['wa'][ilim+npix2a],color='blue',lw=0,alpha=0.2,zorder=1)
        ax.plot(data['wa'][istart:iend],data['fl'][istart:iend],'black',lw=0.7)
        ax.plot(data['wa'][istart:iend],data['er'][istart:iend],'cyan',lw=0.7)
        ax.axhline(y=0,ls='dashed',color='k',lw=0.2)
        ax.axvline(x=float(lldla),color='red',lw=2,alpha=0.5)
        ax.axvline(x=float(llhard),color='red',lw=2,alpha=0.5)
        if fit:
            flfit = residual(self.fitparams,wafit)
            ax.plot(wafit,flfit,'red',lw=0.7)
        else:
            flux = 1.
            for Ntrans in range (len(HIlist)):
                flux = flux*p_voigt(N,b,wafit/(zalpha+1),HIlist[Ntrans]['wave'],HIlist[Ntrans]['gamma'],HIlist[Ntrans]['strength'])
                flux  = gaussian_filter1d(flux,1.5)
            ax.plot(wafit,flux,'red',lw=0.7)
    
    def specplot(self,data,zalpha,llhard,Ncol=1,Nplot=2,**kwargs):
        '''
        Plot the spectrum region from the detected Lyman-limit to the detected Lyman-alpha
    
        Parameters
        ----------
        fig   : Figure to be plotted on
        Ncol  : Number of columns to plot over
        Nplot : Plot number
        '''
        Nrows  = 23
        istart = abs(data['wa'] - ((zalpha+1)*HIlist[-1]['wave']-20)).argmin()
        iend   = abs(data['wa'] - ((zalpha+1)*HIlist[0]['wave']+20)).argmin()
        ymin,ymax = self.set_ylimit(data['fl'],istart,iend)
        x = data['wa'][istart:iend]
        y = data['fl'][istart:iend]        
        ax = self.fig.add_subplot(Nrows,Ncol,Nplot,xlim=[data['wa'][istart],data['wa'][iend]],ylim=[ymin,ymax])
        ax.yaxis.set_major_locator(ticker.NullLocator())
        ax.xaxis.set_major_locator(ticker.NullLocator())
        ax.axhline(y=0,ls='dashed',color='k',lw=0.2)
        ax.plot(x,y,'black',lw=0.2)
        ax.plot(x,data['er'][istart:iend],'cyan',lw=0.2)
        for trans in HIlist:
            ax.axvline(x=(zalpha+1)*trans['wave'], color='red', lw=0.5)
        ax.axvline(x=llhard, color='lime', lw=1)
        xmin = 10*round(min(x)/10)
        xmax = 10*round(max(x)/10)
        if 10*round((xmax-xmin)/100)>0:
            ax.xaxis.set_major_locator(plt.FixedLocator(numpy.arange(xmin,xmax,10*round((xmax-xmin)/100))))
        else:
            ax.xaxis.set_major_locator(plt.FixedLocator([xmin,xmax]))            
    
    def Hplot(self,data,fit,Ntrans,zalpha,dlawidth,llhard,N,b,nleftHI=None,nrightHI=None,vmin=-1200.,vmax=3000.,Ncol=2,Nplot=10,**kwargs):
        '''
        Plot the HI lines
    
        Parameters
        ----------
        fig    : Figure to be plotted on
        Ntrans : Transition number
        vmin   : Minimum velocity on plot in km/s
        vmax   : Maximum velocity on plot in km/s
        Ncol   : Number of columns to plot over
        Nplot  : Plot number
        '''
        zmin    = zalpha * (2*c-dlawidth/2) / (2*c+dlawidth/2) - dlawidth / (2*c-dlawidth/2)
        zmax    = zalpha * (2*c+dlawidth/2) / (2*c-dlawidth/2) + dlawidth / (2*c-dlawidth/2)
        Nrows   = 36
        watrans = HIlist[Ntrans]['wave']*(zalpha+1) #observed wavelength of transition
        wabeg   = watrans*(1+vmin/c)
        waend   = watrans*(1+vmax/c)
        istart  = abs(data['wa']-wabeg).argmin()
        iend    = abs(data['wa']-waend).argmin()
        istart  = istart-1 if istart!=0 else istart
        iend    = iend+1
        v       = c*((data['wa']-watrans)/watrans)
        vllobs  = c*((llhard-watrans)/watrans)
        ymin,ymax = self.set_ylimit(data['fl'],istart,iend)
        ax = self.fig.add_subplot(Nrows,Ncol,Nplot,xlim=[vmin,vmax],ylim=[ymin,ymax])
        ax.yaxis.set_major_locator(ticker.NullLocator())
        ax.plot(v[istart:iend],data['fl'][istart:iend],'black',lw=0.2)
        ax.plot(v[istart:iend],data['er'][istart:iend],'cyan',lw=0.2)
        ax.axhline(y=0,ls='dashed',color='k',lw=0.2)
        ax.axvline(x=0,ls='dashed',color='blue',lw=0.5,alpha=0.7)
        ax.axvline(x=llhard,color='red',lw=2,alpha=0.5)
        if Ntrans<30:
            ax.xaxis.set_ticklabels([])
        if zmin!=zmax:
            wamin = HIlist[Ntrans]['wave']*(zmin+1)
            wamax = HIlist[Ntrans]['wave']*(zmax+1)
            vmin2 = c*(2*(wamin-watrans)/(wamin+watrans))
            vmax2 = c*(2*(wamax-watrans)/(wamin+watrans))
            ax.axvline(x=vmin2,color='blue',lw=1,alpha=0.7)
            ax.axvline(x=vmax2,color='blue',lw=1,alpha=0.7)
            # Plotting the edges found of the system
            if nleftHI!=None and Ntrans==nleftHI:
                ax.axvspan(vmin2,0,color='lime',lw=1)
            if nrightHI!=None and Ntrans==nrightHI:
                ax.axvspan(0,vmax2,color='lime',lw=1)
        if fit:
            y = numpy.ma.array(data['fl'],mask=1-self.fitmask)
            ax.plot(v,y,'magenta',lw=3,alpha=0.3)
            wafit = numpy.arange(wabeg,waend,0.1)
            v = c*((wafit-watrans)/watrans)
            flfit = residual(self.fitparams,wafit)
            ax.plot(v,flfit,'red',lw=0.7,alpha=0.7)
        else:
            # Plotting first guesses
            wafit = numpy.arange(wabeg,waend,0.05)
            v = c*((wafit-watrans)/watrans)
            flux = p_voigt(N,b,wafit/(zalpha+1),HIlist[Ntrans]['wave'],HIlist[Ntrans]['gamma'],HIlist[Ntrans]['strength'])
            ax.plot(v,flux,'red',lw=0.7,alpha=0.7)
    
    def metalplot(self,data,Ntrans,zalpha,dlawidth,vmin=-500.,vmax=500.,Ncol=2,Nplot=11,**kwargs):
        '''
        Plot the HI lines
    
        Parameters
        ----------
        fig    : Figure to be plotted on
        Ntrans : Transition number
        vmin   : Minimum velocity on plot in km/s
        vmax   : Maximum velocity on plot in km/s
        Ncol   : Number of columns to plot over
        Nplot  : Plot number
        '''
        zmin    = zalpha * (2*c-dlawidth/2) / (2*c+dlawidth/2) - dlawidth / (2*c-dlawidth/2)
        zmax    = zalpha * (2*c+dlawidth/2) / (2*c-dlawidth/2) + dlawidth / (2*c-dlawidth/2)
        Nrows   = 36
        watrans = (zalpha+1.)*Metallist[Ntrans]['Metalwave']
        v       = (c*((data['wa']-watrans)/data['wa']))
        istart  = abs(v-vmin).argmin()
        iend    = abs(v-vmax).argmin()
        istart  = istart-1 if istart!=0 else istart
        iend    = iend+1
        y       = data['fl'][istart:iend]
        ymin,ymax = self.set_ylimit(data['fl'],istart,iend)
        ax = self.fig.add_subplot(Nrows,Ncol,Nplot,xlim=[vmin,vmax],ylim=[ymin,ymax])
        ax.yaxis.set_major_locator(ticker.NullLocator())
        ax.axhline(y=0,ls='dashed',color='k',lw=0.2)
        ax.plot(v[istart:iend],y,'black',lw=0.2)
        ax.plot(v[istart:iend],data['er'][istart:iend],'cyan',lw=0.2)
        ax.text(0.9*vmin,0.2*ymax,Metallist[Ntrans]['Metalline']+'_'+str(int(Metallist[Ntrans]['Metalwave'])),color='blue',fontsize=7)
        ax.axvline(x=0, ls='dotted', color='blue', lw=0.2)
        if Ntrans<30:
            ax.xaxis.set_ticklabels([])
        if zmin!=zmax:
            wamin = Metallist[Ntrans]['Metalwave']*(zmin+1)
            wamax = Metallist[Ntrans]['Metalwave']*(zmax+1)
            vmin2 = c*((wamin-watrans)/watrans)
            vmax2 = c*((wamax-watrans)/watrans)
            ax.axvline(x=vmin2,color='blue',lw=0.2)
            ax.axvline(x=vmax2,color='blue',lw=0.2)
    
    def set_ylimit(self,fl,istart,iend):
        ymin,ymax = 0,1
        if istart!=iend:
            ymax = 1.1*sorted(fl[istart:iend])[int(0.99*(iend-istart))]
            ymin = -ymax*0.1/1.1
        return ymin,ymax

def plot_dla(spectrum,resolution,save=False,**kwargs):
    data = readspec(spectrum,resolution)
    qsoname = re.split(r'[/.]',spectrum)[-2]
    PlotDLA(qsoname,data,save=save,**kwargs)
    
