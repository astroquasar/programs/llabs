# External
import numpy
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import plotly.graph_objects as go
from scipy.optimize import curve_fit
from matplotlib.backends.backend_pdf import PdfPages
from plotly.subplots import make_subplots

def gauss(x, a, b, c):
    return a * numpy.exp(-(x - b)**2.0 / (2 * c**2))

def llhist(data,output=None,binning=47,limit=[0,10000],fit=False,**kwargs):
    fig = plt.figure(figsize=(12,7),dpi=100)
    plt.style.use('seaborn')
    plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95, hspace=0.15, wspace=0.2)
    ax   = plt.subplot(111,xlim=limit)
    idxs = numpy.where(numpy.logical_and(data.llshift>limit[0],data.llshift<limit[1]))[0]
    hist = ax.hist(data.llshift[idxs],bins=binning,stacked=True,fill=True,
                   alpha=0.4,edgecolor="black",range=limit)
    best = numpy.where(data.snr>10)[0]
    ax.hist(data.llshift[best],bins=binning,stacked=True,fill=True,
            alpha=0.4,color='red',edgecolor="black",range=limit)
    if fit:
        x = numpy.array([0.5 * (hist[1][i] + hist[1][i+1]) for i in range(len(hist[1])-1)])
        y = hist[0]
        popt, pcov = curve_fit(gauss,x,y,p0=[20,2000,500])
        x = numpy.arange(limit[0],limit[1],0.1)
        y = gauss(x, *popt)
        plt.plot(x, y, lw=3, color="r")
    ax.get_xaxis().get_major_formatter().set_scientific(False)
    ax.set_xlabel('Apparent shift in the Lyman limit break (km/s)')
    ax.set_ylabel('Frequency')
    plt.tight_layout()
    plt.show() if output==None else plt.savefig(output)

def llscatter(data,output=None,binning=47,xcolor=None,**kwargs):
    plt.style.use('seaborn')
    fig = plt.figure(figsize=(7, 5),dpi=100)
    gs = fig.add_gridspec(2, 2,  width_ratios=(7, 2), height_ratios=(2, 7),
                          left=0.1, right=0.97, bottom=0.1, top=0.97,
                          wspace=0.05, hspace=0.05)
    ax = fig.add_subplot(gs[1, 0])
    ax_histx = fig.add_subplot(gs[0, 0], sharex=ax)
    ax_histy = fig.add_subplot(gs[1, 1], sharey=ax)
    ax_histx.tick_params(axis="x", labelbottom=False)
    ax_histy.tick_params(axis="y", labelleft=False)
    (vmin,vmax) = (None,None) if xcolor==None else (xcolor[0],xcolor[-1])
    im = ax.scatter(data.llshift,data.dlawidth,c=data.snr,edgecolors='k',cmap='rainbow',vmin=vmin,vmax=vmax,s=50,lw=0.5)
    ax.set_xlabel('Apparent shift in the Lyman limit break (km/s)')
    ax.set_ylabel('Detected DLA velocity width (km/s)')
    ax_histx.hist(data.llshift,rwidth=0.9,bins=binning,histtype='bar',color='0.2')
    ax_histy.hist(data.dlawidth,rwidth=0.9,bins=binning,histtype='bar',color='0.2',orientation='horizontal')
    cax = plt.axes([0.8, 0.82, 0.15, 0.03])
    cbar = plt.colorbar(im,cax=cax,orientation='horizontal')
    cbar.set_label("Signal-to-noise", labelpad=7)
    if xcolor!=None:
        cbar.set_ticks(xcolor)
        cbar.set_ticklabels(xcolor)
    cbar.ax.xaxis.set_ticks_position('top')
    cbar.ax.xaxis.set_label_position('top')
    plt.show() if output==None else plt.savefig(output)

def snrplot(data,output=None,binning=47,**kwargs):
    fig = plt.figure(figsize=(12,7),dpi=100)
    plt.style.use('seaborn')
    plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95, hspace=0.15, wspace=0.2)
    plt.hist(data.snr,bins=binning,stacked=True,fill=True,edgecolor="black")
    plt.xlabel('Signal-to-noise ratio')
    plt.tight_layout()
    plt.show() if output==None else plt.savefig(output)
    
def interplot(data,size=15,output=None,**kwargs):
    fig = go.Figure(
        data=go.Scatter(
            x=data.llshift,
            y=data.dlawidth,
            mode='markers',
            customdata=data[['qso','snr']].to_numpy(),
            hovertemplate= \
            'Quasar spectrum name : %{customdata[0]}<br>' + \
            'Lyman limit apparent shift : %{x:.2f}<br>' + \
            'DLA velocity dispersion : %{y:.2f}<br>' + \
            'Signal-to-noise ratio : %{customdata[1]:,}<br>' + \
            '<extra></extra>',
            marker=dict(
                size=size,
                color=data.snr,
                colorbar=dict(
                    thickness=20,
                    title='Signal-to-noise ratio',
                    titleside='right'
                ),
                line=dict(width=3,color='white'),
            ),
        )
    )
    fig.update_layout(
        autosize=True,
        xaxis_title='Apparent shift in the Lyman limit break (km/s)',
        yaxis_title='Detected DLA velocity width (km/s)',
        font=dict(size=15),
        margin=dict(l=0, r=0, b=0, t=0),
    )
    if output!=None:
        fig.write_html(output)
    else:
        fig.show()
