# External
import numpy
import scipy
from lmfit import minimize, Parameters, report_fit

# Local
from .voigt import p_voigt
from ..constants import *

def fitHI(data,zalpha,dlawidth,nleftHI,nrightHI,llhard,lldla,N,b,**kwargs):
    """
    Using lmfit to fit a one component model to the absorber
    """
    zmin = zalpha * (2*c-dlawidth/2) / (2*c+dlawidth/2) - dlawidth / (2*c-dlawidth/2)
    zmax = zalpha * (2*c+dlawidth/2) / (2*c-dlawidth/2) + dlawidth / (2*c-dlawidth/2)
    masks = maskdef(data,zalpha,zmin,zmax,nleftHI,nrightHI,**kwargs)
    transmin = 0 #lowest transition to be fitted, alpha=0
    transmax = 30 # highest transition to be fitted, max 31
    voigtparams = Parameters()
    voigtparams.add('z', value=zalpha, min=0.9*zalpha, max=1.1*zalpha,vary=True)
    voigtparams.add('N', value=N, min=0,max=30,vary=False)
    voigtparams.add('b', value=b,min=1.,max=200,vary=True)
    voigtparams.add('transmin',value=transmin,min=0,max=31,vary=False)
    voigtparams.add('transmax',value=transmax,min=0,max=30,vary=False)
    guessparams = voigtparams
    fitmask = numpy.ndarray((len(data['wa']),),dtype=bool)
    fitmask.fill(0)
    fitmask[masks[0]:masks[1]] = 1
    fitmask[masks[2]:masks[3]] = 1
    out = minimize(residual, voigtparams, args=(data['wa'][fitmask],data['fl'][fitmask],data['er'][fitmask]))
    fitchi2 = out.redchi
    fitparams = voigtparams
    zalpha  = fitparams['z'].value
    N       = fitparams['N'].value
    b       = fitparams['b'].value
    waalpha = (1.+zalpha)*HIlist[0]['wave']
    lldla = (zalpha+1.)*HIlist[-1]['wave']
    dvshift = 2*(llhard-lldla)/(llhard+lldla)*c
    return fitmask, fitparams

def maskdef(data,zalpha,zmin,zmax,nleftHI,nrightHI,irange,stdev,maskwidth=0.5,**kwargs):
    ileftmasklow  = abs(data['wa'] - HIlist[nleftHI]['wave']*(zmin+1)).argmin()
    ileftmaskhigh = abs(data['wa'] - HIlist[nleftHI]['wave']*(zalpha+1)).argmin()
    for i in range(ileftmaskhigh,irange,-1):
        if numpy.average(data['fl'][i-irange:i+irange]/data['er'][i-irange:i+irange]) > stdev:
            ileftmasklow = i-int(maskwidth*(ileftmaskhigh-ileftmasklow))
            break
    irightmasklow  = abs(data['wa'] - HIlist[nrightHI]['wave']*(zalpha+1)).argmin()               
    irightmaskhigh = abs(data['wa'] - HIlist[nrightHI]['wave']*(zmax+1)).argmin()
    for i in range(irightmasklow,len(data['wa']),1):
        if numpy.average(data['fl'][i-irange:i+irange]/data['er'][i-irange:i+irange]) > stdev:
            irightmaskhigh = i+int(maskwidth*(irightmaskhigh-irightmasklow))
            break
    return ileftmasklow,ileftmaskhigh,irightmasklow,irightmaskhigh

def read_atom_dat():
    """
    Reads atom.dat file from VPfit into numpy array of strings.
    """
    atomdat  = scipy.array([])
    restwl = []
    transs = []
    gammas = []
    resstr = []
    for line in open(datapath+'atom.dat'):
        if line.startswith('!'):
            continue
        tmp = line.split()
        if len(tmp[0]) == 1:
            tmp[0] += ' ' + tmp[1]
            tmp.pop(1)
        transs.append(tmp[0])
        restwl.append(tmp[1])
        resstr.append(tmp[2])
        gammas.append(tmp[3])
    atomdat = scipy.vstack(
        [scipy.asarray(transs),
         scipy.asarray(restwl),
         scipy.asarray(resstr),
         scipy.asarray(gammas)]).T
    return atomdat

def get_transition_info(transition='Halpha'):
    """
    Collects info on the transition. At later stage may read info from
    VPfit atom.dat and/or my own version of the galaxy lines thingie.
    NB: if the atom.dat file doesn't exist, will fall back on Halpha...
    """
    resstrengths={'Halpha': 0.695800, 'Hbeta': 0.121800, 'Hgamma': 0.044370}
    gammas = {'Halpha': 6.465e7, 'Hbeta': 2.062e7, 'Hgamma': 9.425e6}
    if os.path.exists(datapath+'atom.dat'):
        atomdat = read_atom_dat()
        tmp = transition.split()
        if len(tmp) == 3:
            tmp[0] += ' ' + tmp[1]
            tmp.pop(1)
        name = tmp[0]
        restwl = tmp[1]
        row = scipy.where((atomdat[:, 0] == name) & (atomdat[:, 1] == restwl))
        #f = float(atomdat[row, 2])
        #g = float(atomdat[row, 3])
        f = atomdat[row, 2][0][0]
        g = atomdat[row, 3][0][0]
    else:
        f = resstrengths[transition]
        g = gammas[transition]
    return f, g

def residual(voigtparams, wavelengths, flux=None, error=None):
    """
    Function to use with lmfit. Calculates the weighted residuals of flux-voigtprofile/error.
    voigtparams:   A parameter class defining the variables
    wavelengths:   An array of wavelengths to fit over
    flux:          Corresponding flux values. If absent, the model (and not the residuals) is returned. Useful for plotting.
    error:         Corresponding error values. If absent, the residuals are unweighted
    """
    wall = 911.753463416  #theoretical calculation (http://physics.nist.gov/PhysRefData/HDEL/data.html
    aLL = 6.30e-18 #ask Vincent if there is a newer value
    # Look at derivatives, stepsizes etc.
    z = voigtparams['z'].value                  #redshift
    N = voigtparams['N'].value                  #log(N), column density
    b = voigtparams['b'].value                  #b parameter in km/s
    transmin = voigtparams['transmin'].value    #Lowest transition in transnames to fit
    transmax = voigtparams['transmax'].value    #Highest transition in transnames to fit
    absmodel = numpy.ndarray((len(wavelengths),),dtype=float)
    absmodel.fill(1.)
    restwave = wavelengths/(1+z)
    for trans in transnames[transmin:transmax]:
        f, gamma = get_transition_info(trans)
        f, gamma = float(f), float(gamma)
        lambdao = float(trans.split()[-1])
        absmodel = absmodel*p_voigt(N,b,restwave,lambdao,gamma,f)
        #interpolating between the Lyman Limit and the last line in the Lyman series
    if (transmax == len(transnames)) and (restwave[0] <= wall):
        walast = float(transnames[-1].split()[-1])
        ilast = abs(restwave-walast).argmin()
        ill = abs(restwave-wall).argmin()
        nsubbins = 1000
        subwav = numpy.linspace(wall,walast,nsubbins) #generating a wavelength array of nsubbins bins between wall and walast
        submodel = numpy.ndarray((nsubbins,),dtype=float)
        for i in range(0,nsubbins):
            submodel[i] = numpy.exp(-10**N*aLL*(subwav[i]/wall)**3)
            #rebin to match original wavelengths
        funct = scipy.interpolate.splrep(subwav,submodel,s=0)
        interpolated = scipy.interpolate.splev(restwave[ill:ilast],funct,der=0)
        absmodel[ill:ilast] = interpolated
        absmodel[0:ill] = interpolated[0] #setting the model to zero below the lyman limit
        #convolution with Gaussian to account for instrumental resolution (vsig = 2pix ~ 5km/sec)
        absmodel = scipy.ndimage.filters.gaussian_filter1d(absmodel,1.5)
        #The output depends on wether flux or errors are given (if not given, the output can be used for plotting)
    if flux is None:
        return absmodel
    elif error is None:
        return flux-absmodel
    else:
        return (flux-absmodel)/error
