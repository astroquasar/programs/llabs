# System
import math
import logging

# External
import numpy

# Local
from .constants import *

class AutoScan(object):
    """
    Auto scanning algorithm
    """
    def __init__(self,**kwargs):
        self.waalpha = None
        self.lldla = None
        self.llhard = None
        self.dvshift = None
        self.dlawidth = None
        self.zalpha = None
        self.zlimit = None
        self.iend = None

    def llfind(self,resolution,data,npix1a,npix1b,npix2a,npix2b,threshold1,threshold2,vstart=2000,stdev=2.5,irange=10,**kwargs):
        '''
        Looping over spectrum to find lyman limit and corresponding Lyman-alpha.
        '''
        self.stdev = stdev
        self.irange = irange
        self.npix1a = npix1a
        self.npix1b = npix1b
        self.npix2a = npix2a
        self.npix2b = npix2b
        # Check if DLA search is activated or not
        wastart = data['wa'][0] * (2*c+vstart) / (2*c-vstart)
        istart = 0 if resolution=='low' else abs(data['wa']-wastart).argmin()
        iend = len(data['wa'])
        ill = None
        for i in range(istart+self.npix1b,iend):
            # Discard pixels with high error in window head region
            fl1,er1 = data['fl'][i:i+self.npix1a],abs(data['er'][i:i+self.npix1a])
            badpix  = numpy.where(er1>10000)[0]
            fl1,er1 = numpy.delete(fl1,badpix,0),numpy.delete(er1,badpix,0)
            # Discard pixels with high error in window tail region
            fl2,er2 = data['fl'][i-self.npix1b:i],abs(data['er'][i-self.npix1b:i])
            badpix  = numpy.where(er2>10000)[0]
            fl2,er2 = numpy.delete(fl2,badpix,0),numpy.delete(er2,badpix,0)
            # Calculate metrics
            avgfl1  = numpy.median(fl1)
            avgfl2  = numpy.median(fl2)
            avger1  = sum(er1**2)/self.npix1a**2
            avger2  = sum(er2**2)/self.npix1b**2
            cond1   = (avgfl1-avgfl2)/numpy.sqrt(avger1+avger2) > threshold1
            #quad_sum_error = numpy.sqrt(sum(er1**2+er2**2))/(self.npix1a+self.npix1b)
            #cond1   = (avgfl1-avgfl2)/quad_sum_error > threshold1
            cond2   = avgfl1/numpy.median(er1) > threshold2*avgfl2/numpy.median(er2)
            if cond1==cond2==True:
                ill = i
                self.llfuzzy = data['wa'][ill]
                threshold1, threshold2 = 7, 5
                for j in range(ill,ill+self.npix1a):
                    fl1,er1 = data['fl'][j:j+self.npix2a],abs(data['er'][j:j+self.npix2a])
                    badpix  = numpy.where(er1>10000)[0]
                    fl1,er1 = numpy.delete(fl1,badpix,0),numpy.delete(er1,badpix,0)
                    fl2,er2 = data['fl'][j-self.npix2b:j],abs(data['er'][j-self.npix2b:j])
                    badpix  = numpy.where(er2>10000)[0]
                    fl2,er2 = numpy.delete(fl2,badpix,0),numpy.delete(er2,badpix,0)
                    if len(er1)==0 or len(er2)==0:
                        continue
                    avgfl1  = numpy.median(fl1)
                    avgfl2  = numpy.median(fl2)
                    avger1  = sum(er1**2)/self.npix2a**2
                    avger2  = sum(er2**2)/self.npix2b**2
                    cond1   = (avgfl1-avgfl2)/numpy.sqrt(avger1+avger2) > threshold1
                    #quad_sum_error = numpy.sqrt(sum(er1**2)+sum(er2**2))/(self.npix2a+self.npix2b)
                    #cond1   = (avgfl1-avgfl2)/quad_sum_error > threshold1
                    if resolution=='low':
                        cond2 = avgfl1/numpy.median(er1) > threshold2*avgfl2/numpy.median(er2)
                    else:
                        cond2 = (data['wa'][ill]/HIlist[-1]['wave'])-1.< threshold1
                    if cond1==cond2==True:
                        self.llhard = data['wa'][j]
                        flag = 'llfound'
                        self.zlimit = (self.llhard/HIlist[-1]['wave'])-1.
                        logging.debug('|- Lyman Limit found :')
                        logging.debug('|  |- Fuzzy limit : %.2f A' % self.llfuzzy)
                        logging.debug('|  |- Hard limit  : %.2f A' % self.llhard)
                        logging.debug('|  |- LL redshift : %.5f' % self.zlimit)
                        self.findsatreg(data,**kwargs)
                        break
                if self.dvshift!=None:
                    break
        if self.dvshift==None:
            self.llhard = None
            logging.info('|- No suitable absorption system found')
        else:
            logging.info('|- Lyman Limit found :')
            logging.info('|  |- Fuzzy limit : %.2f A' % self.llfuzzy)
            logging.info('|  |- Hard limit  : %.2f A' % self.llhard)
            logging.info('|  |- LL redshift : %.5f' % self.zlimit)
            logging.info('|- Best DLA candidate:')
            logging.info('|  |- Absorption redshift    : %.5f' % self.zalpha)
            logging.info('|  |- Lyman limit wavelength : %.2f A' % self.lldla)
            logging.info('|  |- Velocity width         : %.2f km/s' % self.dlawidth)
            logging.info('|  |- Minimum redshift       : %.5f ( left wing found at Ly %i )' % (self.zmin,self.nleftHI))
            logging.info('|  |- Maximum redshift       : %.5f ( right wing found at Ly %i )' % (self.zmax,self.nrightHI))
            logging.info('|  |- Column density         : %.2f ( %.2f km/s wide Lyman-alpha )' % (self.N,self.widthfirstHI))
            logging.info('|  |- Doppler parameter      : %.2f ( %.2f km/s wide Lyman %i )' % (self.b,self.dlawidth,self.lastHI))
            logging.info('|  |- LL apparent shift      : %.2f km/s' % self.dvshift)
                
    def findsatreg(self,data,limshift=10000,dvtrans=50,dvlyamax=20000,padstart=50,**kwargs):
        ''' 
        Look for Lyman alpha signature of DLA type systems by looking for wide velocity absorption.
        '''
        logging.debug('|- Looking for DLA Lyman-alpha region(s):')
        flag = 0
        dlalist = []
        self.zmin, self.zmax, self.lldla = None, None, None
        # Wavelength of the associated Lyman-alpha
        self.waalpha = HIlist[0]['wave']*(self.zlimit+1.)+padstart
        # Pixel position of the associated Lyman-alpha
        istart = abs(data['wa'] - self.waalpha).argmin()
        if self.iend==None:
            # Define minimum redshift to scan the spectrum for Lyman-alpha
            dv = -limshift
            zll = self.zlimit * (2*c+dv) / (2*c-dv) + 2*dv / (2*c-dv)
            # Wavelength of the associated Lyman-limit
            walimit = HIlist[0]['wave']*(zll+1.)
            # Pixel position of the associated Lyman limt
            self.iend = abs(data['wa'] - walimit).argmin()
        # Initialise array to record all likely candidates
        likely = numpy.empty((0,4))
        # Scan spectrum blueward and look for DLA type Lyman alpha absorption
        for i in range(istart-self.irange,self.iend,-1):
            # Median value over self.irange below current position
            medianleft = numpy.median(data['fl'][i-self.irange:i])
            # Median value over self.irange above current position
            medianright = numpy.median(data['fl'][i:i+self.irange])
            avgrange = numpy.average(data['fl'][i-self.irange:i+self.irange]/data['er'][i-self.irange:i+self.irange])
            # If right wing found
            if (flag==0) and (avgrange < self.stdev) and (medianleft < data['fl'][i] < medianright):
                # Set the new right edge pixel position
                iright = i
                # Flag position
                flag = 1
            # If left wing found
            if (flag==1) and (avgrange > self.stdev) and (medianleft > data['fl'][i] > medianright):
                # Set the new left edge pixel position
                ileft = i
                # Flag position
                flag = 2
                # Calculate system redshift
                self.zalpha = ((data['wa'][ileft]+data['wa'][iright])/2)/HIlist[0]['wave']-1
                # Velocity interval
                velint = c*(2*(data['wa'][iright]-data['wa'][ileft])/(data['wa'][iright]+data['wa'][ileft]))
                # Condition 1 for Lyalpha detection: decently large velocity dispersion
                cond1 = dvtrans < velint < dvlyamax
                # Condition 2 for Lyalpha detection: average flux/error less than stdev
                cond2 = numpy.average(data['fl'][ileft:iright]/data['er'][ileft:iright]) < self.stdev
                cond3 = self.zalpha not in dlalist
                cond4 = abs(2 * (self.zalpha-self.zlimit) / (self.zalpha+self.zlimit+2) * c) < limshift
            # If DLA type Lyman alpha found
            if (flag==2) and cond1 and cond2 and cond3 and cond4:
                logging.debug('|  |- Saturated Lyman-alpha region found at %.5f with dv = %.2f km/s' % (self.zalpha,velint))
                logging.debug('|  |  |- Right wing found at %.2f A with average SNR of %.2f' % (data['wa'][i],avgrange))
                logging.debug('|  |  |- Left wing found at %.2f A with average SNR of %.2f' % (data['wa'][i],avgrange))
                # Add system to list of possible candidates
                likely = numpy.vstack([likely,[self.zalpha,velint,ileft,iright]])
                # DLA redshift recorded for future loop
                (dlalist).append(self.zalpha)
                # Discard the system and stop looking for the left edge
                flag = 'dlafound'
                flag = 0
            elif flag==2:
                # If velocity width smaller than minimum DLA type width (i.e. N=19.5, b=10),
                # discard the system and stop looking for the left edge
                flag = 0
        # If no suitable system found
        if len(likely)==0:
            logging.debug('|  |- No suitable DLA Lyman-alpha found!')
        else:
            self.evaluate_region(likely,data,dvtrans,**kwargs)
        self.iend = istart

    def evaluate_region(self,likely,data,dvtrans,ly=31,**kwargs):
        logging.debug('|- Evaluating %i saturated Lyman-alpha candidate regions:' % len(likely))
        # Initialise array to record all system candidates
        allsys = numpy.empty((0,4),dtype=object)
        for idx in range(len(likely)):
            self.zalpha = likely[idx,0]
            logging.debug('|  |- Scanning candidate region %i at redshift %.5f' % (idx+1,self.zalpha))
            ileft = int(likely[idx,2])
            iright = int(likely[idx,3])
            '''
            Once Lyman alpha found, look within its redshift range for a saturated system over nHI HI transitions.
            Check, in that order, if the transition is:
            1) outside the spectral range (flag=1) > system recorded based on previous transitions found
            2) noisy (flag=2) > system recorded based on previous transitions found
            3) not saturated (flag=-1) > system discarded
            '''
            # Initialise array to record all system candidates
            system = numpy.empty((0,4),dtype=object)
            # End of the scan (left part of the detected Lyman alpha transition)
            imin = ileft
            # Start of the scan (right part of the detected Lyman alpha transition)
            # The starting pixel is located dvtrans km/s leftward the detected
            # Lya right edge.
            imax = abs(data['wa']-data['wa'][iright]*(1-dvtrans/c)).argmin()
            # Initialise Lya left edge as upper limit for future right edges
            iprev = iright+2
            # Scan spectrum blueward and look for DLA type Lyman alpha absorption
            for i in range(imax,imin,-1):
                # Initialise flag for detected absorption systems
                flag = 0
                Lya_zmid = data['wa'][i]/HIlist[0]['wave']-1
                Lya_wmin = data['wa'][i]*(1-dvtrans/c)
                Lya_wmax = data['wa'][i]*(1+dvtrans/c)
                Lya_zmin = Lya_wmin/HIlist[0]['wave']-1
                Lya_zmax = Lya_wmax/HIlist[0]['wave']-1
                # Pixel of the new left edge, dvtrans blueward the new Lya pixel position
                Lya_imin = abs(data['wa']-Lya_wmin).argmin()
                # Pixel of the new right edge, dvtrans redward the new Lya pixel position
                Lya_imax = abs(data['wa']-Lya_wmax).argmin()
                #logging.debug('|  |  |- Scanning sub-region around redshift %.5f' % Lya_zmid)
                # Loop over HI transitions
                for j in range(ly):
                    HI_wmid = data['wa'][i]/HIlist[0]['wave']*HIlist[j]['wave'] 
                    HI_wmin = HI_wmid*(1-dvtrans/c)
                    HI_wmax = HI_wmid*(1+dvtrans/c)
                    # Pixel index on the blue edge
                    HI_imin = abs(data['wa']-HI_wmin).argmin()
                    # Pixel index on the red edge
                    HI_imax = abs(data['wa']-HI_wmax).argmin()
                    npixsat = len(numpy.where(numpy.array(data['fl'][HI_imin:HI_imax]/data['er'][HI_imin:HI_imax])<self.stdev)[0])
                    # Exploring Lyman order
                    if  HI_imax-HI_imin<10:
                        break
                    elif data['wa'][HI_imin]>HI_wmax or data['wa'][HI_imax]<HI_wmin or HI_imin==0:
                        break
                    elif npixsat<0.4*(HI_imax-HI_imin):
                        # More than 20% of the pixels have average flux/error larger than stdev
                        # 100*npixsat/(HI_imax-HI_imin) percent saturation, flag system as not saturated
                        flag = 1
                        break
                if Lya_imin==Lya_imax:
                    continue
                # If saturated system found
                if flag==0:
                    # If new system found (current right edge below previous left edge)
                    if Lya_imax < iprev and Lya_zmin < self.zlimit:
                        # Record left edge redshift
                        self.zmin = data['wa'][Lya_imin]/HIlist[0]['wave']-1
                        # Record right edge redshift
                        self.zmax = data['wa'][Lya_imax]/HIlist[0]['wave']-1
                        # Left edge wavelength of the candidate system
                        wamin = HIlist[0]['wave']*(self.zmin+1)
                        # Right edge wavelength of the candidate system
                        wamax = HIlist[0]['wave']*(self.zmax+1)
                        # Calculate velocity dispersion
                        self.dlawidth = 2*(wamax-wamin)/(wamax+wamin)*c
                        # Add system to list of possible candidates
                        system = numpy.vstack([system,[j,self.zmin,self.zmax,self.dlawidth]])
                        logging.debug('|  |  |- System found from zmin=%.5f to zmax=%.5f with dv = %.2f km/s' % (self.zmin,self.zmax,self.dlawidth))
                        # Define pixel of current Lya left edge as upper limit for future right edge
                        iprev = Lya_imin
                    # If region connected to previous absorption candidate, extend region
                    if Lya_imax >= iprev and (Lya_zmin+self.zmax)/2 < self.zlimit:
                        # Re-estimate redshift of the left edge
                        self.zmin = data['wa'][Lya_imin]/HIlist[0]['wave']-1
                        # Re-estimate wavelength of the left edge
                        wamin = HIlist[0]['wave']*(self.zmin+1)
                        # Re-estimate velocity dispersion
                        self.dlawidth = 2*(wamax-wamin)/(wamax+wamin)*c
                        # Update system information in list
                        system[-1,:] = [j,self.zmin,self.zmax,self.dlawidth]
                        self.zalpha = (self.zmin+self.zmax)/2
                        logging.debug('|  |  |- System extended from zmin=%.5f to zmax=%.5f with dv = %.2f km/s' % (self.zmin,self.zmax,self.dlawidth))
                        # Define pixel of current Lya left edge as upper limit for future right edge
                        iprev = Lya_imin
            if len(system)==0:
                logging.debug('|  |  |- No suitable saturated Lyman series found')
            else:
                allsys = numpy.concatenate((allsys,system))
        allsys = allsys[allsys[:,3].argsort()][::-1]
        self.getbestz(allsys,data,**kwargs)
        
    def getbestz(self,allsys,data,**kwargs):
        '''
        For all HI transitions available in the spectrum, measure the
        velocity dispersion, and re-estimate the redshift in the closest
        redshift range available.
        '''
        flag = 'dlafound'
        for system in allsys:
            nHI = int(system[0])
            self.zmin = float(system[1])
            self.zmax = float(system[2])
            # Midpoint redshift of the DLA-type system found
            self.zalpha = (self.zmin+self.zmax)/2
            # Midpoint wavelength of the DLA-type system found
            self.waalpha = HIlist[0]['wave']*(self.zalpha+1)
            self.nleftHI = self.nrightHI = nHI if nHI == 0 else nHI-1
            # Loop over all HI transitions down to Lyman order limit
            for j in range(nHI):
                # Re-defining pixel position based on previous value
                imin = abs(data['wa'] - HIlist[j]['wave']*(self.zmin+1)).argmin()
                # Re-defining pixel position based on previous value
                iref = abs(data['wa'] - HIlist[j]['wave']*(self.zalpha+1)).argmin()
                # Re-defining pixel position based on previous value
                imax = abs(data['wa'] - HIlist[j]['wave']*(self.zmax+1)).argmin()
                npix = len(numpy.where(numpy.array(data['fl'][imin:imax]/data['er'][imin:imax])<self.stdev)[0])
                if npix<0.4*(imax-imin):
                    flag = 'llfound'
                    break
                for i in range(iref,imin-2,-1):
                    med = numpy.median(data['fl'][i-self.irange:i+self.irange]/data['er'][i-self.irange:i+self.irange])
                    zmin = data['wa'][i]/HIlist[j]['wave']-1
                    if med > self.stdev:
                        self.zmin = zmin
                        self.nleftHI = j
                        break
                    elif data['wa'][i] < self.llhard:
                        break
                for i in range(iref,imax+2):
                    med = numpy.median(data['fl'][i-self.irange:i+self.irange]/data['er'][i-self.irange:i+self.irange])
                    zmax = data['wa'][i]/HIlist[j]['wave']-1
                    if med > self.stdev:
                        self.zmax = zmax
                        self.nrightHI = j
                        break
                    elif data['wa'][i] < self.llhard:
                        break
                # Re-defining center redshift
                self.zalpha = (self.zmin+self.zmax)/2
            if flag=='dlafound':
                self.lastHI = max([self.nleftHI,self.nrightHI])
                # Left edge wavelength of the candidate system
                wamin = HIlist[self.lastHI]['wave']*(self.zmin+1)
                # Right edge wavelength of the candidate system
                wamax = HIlist[self.lastHI]['wave']*(self.zmax+1)
                # Re-estimate velocity dispersion
                self.dlawidth = 2*(wamax-wamin)/(wamax+wamin)*c
                # Midpoint redshift of the DLA-type system found
                self.zalpha = (self.zmin+self.zmax)/2
                # Midpoint wavelength of the DLA-type system found
                self.waalpha = (self.zalpha+1.)*HIlist[0]['wave']
                self.lldla = (self.zalpha+1.)*HIlist[-1]['wave']
                # LL shift between observed LL (llhard) and associated LL of the detected DLA
                self.dvshift  = 2*(self.llhard-self.lldla)/(self.llhard+self.lldla)*c
                # Central pixel position of the Lyman alpha absorption
                illdla = abs(data['wa'] - self.lldla).argmin()
                dvlldla = 2*abs(data['wa'][illdla]-self.lldla)/(data['wa'][illdla]+self.lldla)*c
                # Find best column density and Doppler parameter estimatse
                self.getparam(data)
                break
                
    def getparam(self,data):
        '''
        Determine the column density and Doppler parameter of the DLA by locate precisely the wings of the Lyman alpha.
        '''
        self.N = float('nan')
        self.b = float('nan')
        # Wavelength of the associated Lyman alpha
        self.waalpha = HIlist[0]['wave']*(self.zalpha+1)
        # Central pixel position of the Lyman alpha absorption
        imidalpha = abs(data['wa'] - self.waalpha).argmin()
        # Initialisation for Lyman alpha wing detection from center
        i,flag = 0,0
        # Starting from the center, search for one of the 2 wings
        while flag==0:
            ileft = imidalpha-i
            avgrange = numpy.average(data['fl'][ileft-self.irange:ileft+self.irange]/data['er'][ileft-self.irange:ileft+self.irange])
            if (avgrange > self.stdev):
                # Calculate width of Lyman alpha
                self.widthfirstHI = 2*(data['wa'][imidalpha+i]-data['wa'][imidalpha-i])/(data['wa'][imidalpha+i]+data['wa'][imidalpha-i])*c
                # Pixel position of detected wing (orange line in plot)
                self.point = ileft
                # Stop loop
                flag = 1
            iright = imidalpha+i
            avgrange = numpy.average(data['fl'][iright-self.irange:iright+self.irange]/data['er'][iright-self.irange:iright+self.irange])
            if (avgrange > self.stdev):
                # Calculate width of Lyman alpha
                self.widthfirstHI = 2*(data['wa'][imidalpha+i]-data['wa'][imidalpha-i])/(data['wa'][imidalpha+i]+data['wa'][imidalpha-i])*c
                # Pixel position of detected wing (orange line in plot)
                self.point = iright
                flag = 1
            i = i + 1
        Ndx = abs(d['HI0'][:,0] - self.widthfirstHI).argmin()
        self.N = float(cols[Ndx])
        bdx = abs(d['HI'+str(self.lastHI)][Ndx,:] - self.dlawidth).argmin()
        self.b = float(dops[bdx])
