Procedure & Parameters
======================

.. currentmodule:: llabs.main

Adjustable parameters
---------------------

.. autofunction:: settings

Looping through spectra
-----------------------

.. autofunction:: scan
