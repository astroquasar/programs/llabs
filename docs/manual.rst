Manual Scanning
===============

Procedure
---------

The manual scanning module can be used to create a benchmark sample of Lyman-limit edges and Damped Lyman alpha systems that have visually been inspected and identified. The sample can then be used to adjust the parameters of the automated scanning algorithm. The little animation below shows what the manual inspection procedure looks like, where the user first manually move towards the Lyman-limit region, then select the position of the edge, and finally identify the left and right wings of the strongest absorption systems found in the spectrum.

.. image:: _images/manual.gif

Module
------
	   
The following class is used to execute the manual scanning:

.. currentmodule:: llabs.manual
.. autoclass:: ManualScan
   :no-inherited-members:

      None

   .. rubric:: Methods Summary

   .. autosummary::

      ~__init__
      ~press
      ~llsearch
      ~plottrans
      ~selreg
      ~spidx

   .. rubric:: Methods Documentation

   .. automethod:: __init__
   .. automethod:: press
   .. automethod:: llsearch
   .. automethod:: plottrans
   .. automethod:: selreg
   .. automethod:: spidx
