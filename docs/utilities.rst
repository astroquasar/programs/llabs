Scanning Utilities
==================

Voigt profile fitting
---------------------

.. currentmodule:: llabs.plots.voigt
		   
.. autofunction:: voigtslow
.. autofunction:: voigt
.. autofunction:: p_voigt

fitting
-------

.. currentmodule:: llabs.plots.fitting
		   
.. autofunction:: maskdef
.. autofunction:: fitHI
.. autofunction:: read_atom_dat
.. autofunction:: get_transition_info
.. autofunction:: residual

utilities
---------

.. currentmodule:: llabs.utils
	     
.. autofunction:: readspec
.. autofunction:: equiwidth
.. autofunction:: snrestimator
.. autofunction:: best

