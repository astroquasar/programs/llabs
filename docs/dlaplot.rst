DLA systems
===========

.. currentmodule:: llabs.plots.dlaplot
.. autoclass:: PlotDLA
   :no-inherited-members:

      None

   .. rubric:: Methods Summary

   .. autosummary::

      ~__init__
      ~lyman_limit
      ~specplot
      ~Hplot
      ~metalplot

   .. rubric:: Methods Documentation

   .. automethod:: __init__
   .. automethod:: lyman_limit
   .. automethod:: specplot
   .. automethod:: Hplot
   .. automethod:: metalplot
