Automated Scanning
==================

.. currentmodule:: llabs.auto
.. autoclass:: AutoScan
   :no-inherited-members:

      None

   .. rubric:: Methods Summary

   .. autosummary::

      ~__init__
      ~llfind
      ~findsatreg
      ~getbestz
      ~getparam

   .. rubric:: Methods Documentation

   .. automethod:: __init__
   .. automethod:: llfind
   .. automethod:: findsatreg
   .. automethod:: getbestz
   .. automethod:: getparam
