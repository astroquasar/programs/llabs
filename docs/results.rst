Statistical results
===================

.. currentmodule:: llabs.plots.statistics

Lyman limit histogram
---------------------
		   
.. autofunction:: llhist

Distribution of Lyman limit
---------------------------

.. autofunction:: llscatter

Signal-to-noise ratio
---------------------
		  
.. autofunction:: snrplot

Interactive figure
------------------
		  
.. autofunction:: interplot
