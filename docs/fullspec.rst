Full spectrum
=============

.. currentmodule:: llabs.plots.fullspec

.. autofunction:: wholespec
