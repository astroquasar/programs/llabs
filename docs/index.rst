.. title:: Docs

.. image:: _images/logo_llabs.png
   :class: no-scaled-link
   :width: 40%

.. raw:: html

   <br>

.. image:: https://img.shields.io/badge/License-MIT-blue.svg
   :target: https://gitlab.com/astroquasar/programs/llabs/-/raw/master/LICENSE
.. image:: https://badge.fury.io/py/llabs.svg
   :target: https://pypi.python.org/pypi/llabs/
.. image:: https://img.shields.io/pypi/dm/llabs
.. image:: https://zenodo.org/badge/DOI/10.5281/zenodo.438085.svg
   :target: https://doi.org/10.5281/zenodo.438085
	    
This documentation aims to provide detailed information regarding the use of the LLabs program. This program was built by `Vincent Dumont <https://vincentdumont.gitlab.io/>`_ and is part of the `Astroquasar <https://astroquasar.gitlab.io/>`_ group's toolset available in our `GitLab group <https://gitlab.com/astroquasar/programs/llabs>`_.

Background
----------

.. image:: _images/2specs.png

Installation
------------

The easiest way to install the software is using the `PyPI package manager <https://pypi.org/project/llabs/>`_ as follows::

  pip install llabs

Reporting issues
----------------

.. raw:: html

   <br>
   <a href="https://gitlab.com/astroquasar/programs/llabs/-/issues" class="button3">
   <font color="#ffffff">Submit a ticket</font>
   </a><br>

If you find any bugs when running this program, please make sure to report them by clicking on the above link and
submit a ticket on the software's official Gitlab repository.

.. toctree::
   :caption: Spectral Scanner
   :hidden:
   
   scanner
   auto
   manual
   utilities
   
.. toctree::
   :caption: Data Visualization
   :hidden:

   llregion
   dlaplot
   fullspec
   results
   
.. toctree::
   :caption: Published Work
   :hidden:

   paper.ipynb
